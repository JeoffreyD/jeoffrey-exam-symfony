<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\User;
use App\Form\MessageType;
use App\Repository\ForumRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    private ForumRepository $forumRepository;
    private EntityManagerInterface $entityManager;

    /**
     * @param ForumRepository $forumRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ForumRepository $forumRepository, EntityManagerInterface $entityManager)
    {
        $this->forumRepository = $forumRepository;
        $this->entityManager = $entityManager;
    }


    #[Route('/', name: 'home')]
    public function index(): Response
    {

        $forums = $this->forumRepository->findAll();

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'forums' => $forums
        ]);
    }

    #[Route('/forum/{id}', name: 'forum_view')]
    public function forum(Request $request, int $id): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $forum = $this->forumRepository->find($id);
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->setCreatedAt(new \DateTime());
            $message->setForum($forum);
            $message->setUser($user);

            $this->entityManager->persist($message);
            $this->entityManager->flush();
        }

        return $this->render('home/forum-view.html.twig', [
            'controller_name' => 'HomeController',
            'form' => $form->createView(),
            'forum' => $forum
        ]);
    }
}
