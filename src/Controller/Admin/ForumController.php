<?php

namespace App\Controller\Admin;

use App\Entity\Forum;
use App\Entity\User;
use App\Form\ForumType;
use App\Repository\ForumRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ForumController extends AbstractController
{

    private EntityManagerInterface $entityManager;
    private ForumRepository $forumRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ForumRepository $forumRepository
     */
    public function __construct(EntityManagerInterface $entityManager, ForumRepository $forumRepository)
    {
        $this->entityManager = $entityManager;
        $this->forumRepository = $forumRepository;
    }

    #[Route('/admin/forum', name: 'admin_forum')]
    public function index(): Response
    {

        $forums = $this->forumRepository->findAll();

        return $this->render('admin/forum/index.html.twig', [
            'controller_name' => 'ForumController',
            'forums' => $forums
        ]);
    }

    #[Route('/admin/forum/new', name: 'admin_new_forum')]
    public function newForum(Request $request): Response
    {

        $forum = new Forum();
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(ForumType::class, $forum);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $forum->setCreatedAt(new \DateTime());
            $forum->setUser($user);

            $this->entityManager->persist($forum);
            $this->entityManager->flush();

            return $this->redirectToRoute('admin_forum');
        }

        return $this->render('admin/forum/add.html.twig', [
            'controller_name' => 'ForumController',
            'form' => $form->createView(),
        ]);
    }

    #[Route('/admin/forum/edit/{id}', name: 'admin_edit_forum')]
    public function edit(Request $request, Forum $forum): Response
    {

        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(ForumType::class, $forum);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $forum->setUser($user);

            $this->entityManager->persist($forum);
            $this->entityManager->flush();

            return $this->redirectToRoute('admin_forum');
        }

        return $this->render('admin/forum/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/admin/forum/delete/{id}', name: 'admin_delete_forum')]
    public function delete(Forum $forum): Response
    {
        $this->entityManager->remove($forum);
        $this->entityManager->flush();

        return $this->redirectToRoute('admin_forum', [
        ]);
    }
}
